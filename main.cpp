#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <cmath>

#include <iostream>
#include <chrono>
#include <tuple>
#include <stdexcept>
#include <initializer_list>
#include <thread>



class ToneGen {
    float sinPos,
          sinStep;

    static void populate(void *v_me, Uint8 *stream, int len) {
        auto me = reinterpret_cast<ToneGen*>(v_me);
        int i=0;
        for (i=0; i<len; i++) {
            // Just fill the stream with sine!
            stream[i] = (me->settings.volume * sinf(me->sinPos)) + me->settings.volume;
            me->sinPos += me->sinStep;
        }
    }

    void open() {
        SDL_AudioSpec spec;

        // Set up the requested settings
        spec.freq = settings.baseFreq;
        spec.format = AUDIO_U8;
        spec.channels = 1;
        spec.samples = settings.samples;
        spec.callback = (*populate);
        spec.userdata = this;

        // Open the audio channel
        if (SDL_OpenAudio(&spec, NULL) < 0) {
            throw std::runtime_error(std::string("Failed to open audio: ")+SDL_GetError());
            fprintf(stderr, "Failed to open audio: %s \n", SDL_GetError());
            exit(1);
        }
    }

    void close() {
        // Close autio channel
        SDL_CloseAudio();
    }

public:
    enum class Note {
        B0 = 31,
        C1 = 33,
        CS1 = 35,
        D1 = 37,
        DS1 = 39,
        E1 = 41,
        F1 = 44,
        FS1 = 46,
        G1 = 49,
        GS1 = 52,
        A1 = 55,
        AS1 = 58,
        B1 = 62,
        C2 = 65,
        CS2 = 69,
        D2 = 73,
        DS2 = 78,
        E2 = 82,
        F2 = 87,
        FS2 = 93,
        G2 = 98,
        GS2 = 104,
        A2 = 110,
        AS2 = 117,
        B2 = 123,
        C3 = 131,
        CS3 = 139,
        D3 = 147,
        DS3 = 156,
        E3 = 165,
        F3 = 175,
        FS3 = 185,
        G3 = 196,
        GS3 = 208,
        A3 = 220,
        AS3 = 233,
        B3 = 247,
        C4 = 262,
        CS4 = 277,
        D4 = 294,
        DS4 = 311,
        E4 = 330,
        F4 = 349,
        FS4 = 370,
        G4 = 392,
        GS4 = 415,
        A4 = 440,
        AS4 = 466,
        B4 = 494,
        C5 = 523,
        CS5 = 554,
        D5 = 587,
        DS5 = 622,
        E5 = 659,
        F5 = 698,
        FS5 = 740,
        G5 = 784,
        GS5 = 831,
        A5 = 880,
        AS5 = 932,
        B5 = 988,
        C6 = 1047,
        CS6 = 1109,
        D6 = 1175,
        DS6 = 1245,
        E6 = 1319,
        F6 = 1397,
        FS6 = 1480,
        G6 = 1568,
        GS6 = 1661,
        A6 = 1760,
        AS6 = 1865,
        B6 = 1976,
        C7 = 2093,
        CS7 = 2217,
        D7 = 2349,
        DS7 = 2489,
        E7 = 2637,
        F7 = 2794,
        FS7 = 2960,
        G7 = 3136,
        GS7 = 3322,
        A7 = 3520,
        AS7 = 3729,
        B7 = 3951,
        C8 = 4186,
        CS8 = 4435,
        D8 = 4699,
        DS8 = 4978,
        REST = 0
    };

    struct Settings {
        int baseFreq = 44100;
        unsigned short samples = 8192;
        float volume = 127;
        time_t duration = 200;
    } settings;

    ToneGen() {
        open();
    }
    ToneGen(const Settings& s)
        : settings(s) {
        open();
    }
    ~ToneGen() {
        close();
    }

    void setFreq(float freq) {
        sinStep = 2 * M_PI * freq / settings.baseFreq;
    }

    void play() {
        // Initialize the position of our sine wave
        sinPos = 0;

        /* Now, run this thing */
        SDL_PauseAudio(0);
        /* Delay for the requested number of seconds */
        std::this_thread::sleep_for(std::chrono::milliseconds(settings.duration));
        /* Then turn it off again */
        SDL_PauseAudio(1);
    }
};


int main(int argc, char* argv[]) {
    ToneGen gen;

    std::initializer_list<std::tuple<ToneGen::Note, uint>> melody = {
      {ToneGen::Note::E5, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::B4, 8},
      {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::A4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8},

      {ToneGen::Note::D5, 4}, {ToneGen::Note::F5, 8}, {ToneGen::Note::A5, 4}, {ToneGen::Note::G5, 8}, {ToneGen::Note::F5, 8},
      {ToneGen::Note::E5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::REST, 4},

      {ToneGen::Note::E5, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::B4, 8},
      {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 8}, {ToneGen::Note::A4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8},

      {ToneGen::Note::D5, 4}, {ToneGen::Note::F5, 8}, {ToneGen::Note::A5, 4}, {ToneGen::Note::G5, 8}, {ToneGen::Note::F5, 8},
      {ToneGen::Note::E5, 4}, {ToneGen::Note::C5, 8}, {ToneGen::Note::E5, 4}, {ToneGen::Note::D5, 8}, {ToneGen::Note::C5, 8},
      {ToneGen::Note::B4, 4}, {ToneGen::Note::B4, 8}, {ToneGen::Note::C5, 8}, {ToneGen::Note::D5, 4}, {ToneGen::Note::E5, 4},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::A4, 4}, {ToneGen::Note::REST, 4},


      {ToneGen::Note::E5, 2}, {ToneGen::Note::C5, 2},
      {ToneGen::Note::D5, 2}, {ToneGen::Note::B4, 2},
      {ToneGen::Note::C5, 2}, {ToneGen::Note::A4, 2},
      {ToneGen::Note::GS4, 2}, {ToneGen::Note::B4, 4}, {ToneGen::Note::REST, 8},
      {ToneGen::Note::E5, 2}, {ToneGen::Note::C5, 2},
      {ToneGen::Note::D5, 2}, {ToneGen::Note::B4, 2},
      {ToneGen::Note::C5, 4}, {ToneGen::Note::E5, 4}, {ToneGen::Note::A5, 2},
      {ToneGen::Note::GS5, 2}
    };

    while (true) {
        for (const auto [note, duration] : melody) {
            gen.setFreq(static_cast<float>(note));
            gen.settings.duration = 1000 / duration;
            gen.play();
            std::this_thread::sleep_for(std::chrono::milliseconds(100/duration));
            std::cout << static_cast<uint>(note) << ' ' << gen.settings.duration << std::endl;
        }
    }
}
